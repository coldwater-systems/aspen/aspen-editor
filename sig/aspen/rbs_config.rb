# frozen_string_literal: true

require 'shellwords'

module Aspen
  RBS_LIBRARIES = ['io-console'].freeze
  RBS_FLAGS = ['-I', 'sig', *RBS_LIBRARIES.flat_map {|lib| ['-r', lib] }].shelljoin
end
