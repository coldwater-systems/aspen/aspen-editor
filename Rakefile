# frozen_string_literal: true

require 'bundler/gem_tasks'
require 'bundler/setup'
require 'rspec/core/rake_task'
require 'rubocop/rake_task'
require 'yard'
require_relative 'sig/aspen/rbs_config'

task default: [:checksigs, :spec, :rubocop]

YARD::Rake::YardocTask.new

RSpec::Core::RakeTask.new(:spec) do |task|
  task.verbose = false
end

RuboCop::RakeTask.new do |task|
  task.options = %w[--except Metrics,RSpec/ExampleLength]
  task.verbose = false
end

desc 'automatically run yard and rspec as files change'
task :dev do
  sh 'bundle install'
  sh 'rerun -p "{**/*.{rb,gemspec},exe/**/*,Gemfile*,Rakefile}" -x rake yard spec'
end

desc 'automatically run rubocop:auto_correct as files change'
task :devformat do
  sh 'rerun -p "{**/*.{rb,gemspec},exe/**/*,Gemfile*,Rakefile}" -x rake rubocop:auto_correct'
end

desc 'Run rbs validate'
task :checksigs do
  sh "rbs #{Aspen::RBS_FLAGS} validate --silent"
end
