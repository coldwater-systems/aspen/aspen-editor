# frozen_string_literal: true

module Aspen
  class Editor
    # A class that holds references to global objects
    class Context
      def initialize(responder:, preferences:)
        @responder = responder
        @preferences = preferences
      end

      attr_reader :responder, :preferences
    end
  end
end
