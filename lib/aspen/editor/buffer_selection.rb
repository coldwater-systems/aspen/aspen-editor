# frozen_string_literal: true

require 'stringio'
require_relative '../pair'

module Aspen
  class Editor
    # Represents the coordinates for some selected text, or the insertion point
    # in the case where the selection has zero width. Note that row and col are
    # zero-indexed, except when calling #to_s
    #
    # The state of this data structure is as follows:
    #    (anchor_row, anchor_col, row, [col], col_virtual)
    # (the Array of BufferLines in the buffer must be supplied in the
    # constructor because it is used for reference in some calculations)
    #
    # anchor_row & anchor_col: For selections, this is the boundary that does
    # not move. If nothing is selected, these are nil.
    #
    # row & col: For selections, this is the boundary that will move. Otherwise,
    # this is the insertion point. `col` is actually derived from `col_virtual`,
    # by constraining it to the current line's length, see below.
    #
    # col_virtual: `col` is limited by the current line length, but we still
    # need to keep track of where it would have been if it weren't limited by
    # line length. When moving the cursor up or down through blank or short
    # lines, the cursor must be able to move back rightward when encountering a
    # longer line. This value is internal to the data structure.
    #
    # See <spec/aspen/editor/passthru-short-line.gif> for a visual
    class BufferSelection
      def initialize(row, col, lines:)
        @anchor_row = row
        @anchor_col = col
        @row = row
        @col_virtual = col
        @lines = lines
      end

      attr_reader :anchor_row, :anchor_col, :row

      def col = [@col_virtual, current_line.length].min
      def current_line = @lines.fetch(row)
      def to_s = "row #{anchor_row + 1}-#{row + 1} col #{anchor_col + 1}-#{col + 1}(#{@col_virtual + 1})"
      def empty? = anchor_row == row && anchor_col == col

      # returns [start, end]
      def bounds
        if row < @anchor_row || (row == @anchor_row && col < @anchor_col)
          [BufferPosition.new(row, col), BufferPosition.new(@anchor_row, @anchor_col)]
        else
          [BufferPosition.new(@anchor_row, @anchor_col), BufferPosition.new(row, col)]
        end
      end

      def move_up
        extend_up
        clear
      end

      def move_down
        extend_down
        clear
      end

      def move_left
        extend_left
        clear
      end

      def move_right
        extend_right
        clear
      end

      def extend_up
        @row -= 1 if @row > 0
      end

      def extend_down
        @row += 1 if @row < @lines.length - 1
      end

      def extend_left
        if col > 0
          @col_virtual = col - 1
        elsif @row > 0
          @row -= 1
          @col_virtual = current_line.length
        end
      end

      def extend_right
        if col < current_line.length
          @col_virtual += 1
        elsif @row < @lines.length - 1
          @row += 1
          @col_virtual = 0
        end
      end

      def move_to_top
        clear
        @row = 0
      end

      def move_to_bottom
        clear
        @row = @lines.length - 1
      end

      def move_to_line_start
        @col_virtual = 0
        clear
      end

      def move_to_line_end
        @col_virtual = current_line.length
        clear
      end

      def row=(row)
        raise ArgumentError, "Expecting (0...#{@lines.length}) but got #{row}" if row < 0 || row >= @lines.length

        @row = row
      end

      def col=(col)
        if col < 0 || col > current_line.length
          raise ArgumentError, "Expecting (0..#{current_line.length}) but got #{col}"
        end

        @col_virtual = col
      end

      def move_to(position)
        extend_to(position)
        clear
      end

      def extend_to(position)
        self.row = position.row
        self.col = position.col
      end

      def clear
        @anchor_col = col
        @anchor_row = row
      end

      def clear_virtual_col
        @col_virtual = col
      end
    end
  end
end
