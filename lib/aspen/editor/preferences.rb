# frozen_string_literal: true

require 'yaml'

module Aspen
  class Editor
    # Values changeable by the user.
    class Preferences
      def initialize(preferences_filename)
        @preferences = File.open(preferences_filename, &YAML.method(:safe_load))
      end

      def [](*, key) = @preferences.dig(*, key)
      def view(*path) = View.new(@preferences, path)

      # The result type of Preferences#view that scopes down an
      # Editor::Preferences to a specific prefix for indexing, such that
      # `preferences.view('something')['specific']`` gives the same result as
      # `preferences['something', 'specific']`.
      class View
        def initialize(preferences, path)
          @preferences = preferences
          @path = path
        end

        def [](*subpath, key) = @preferences.dig(*(@path + subpath), key)
        def view(*subpath) = View.new(@preferences, @path + subpath)
      end
    end
  end
end
