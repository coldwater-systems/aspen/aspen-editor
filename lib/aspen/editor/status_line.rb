# frozen_string_literal: true

require_relative '../output_block'
require_relative '../pair'
require_relative 'buffer_line'

module Aspen
  class Editor
    # The white-background line near the bottom of the screen that reports
    # general status messages, including help text and the line/column
    # indicator.
    class StatusLine
      def initialize
        @active_buffer = nil
        @status = ''
      end

      attr_accessor :active_buffer, :status

      def output(size)
        output = OutputBlock.new(size)

        right = @active_buffer ? " #{@active_buffer&.status} " : ''
        left_width = size.width - right.length

        output.print " #{@status}".ljust(left_width)[...left_width], :status_line
        output.puts right, :status_line
        output
      end

      def on_mouse_input(_mouse)
        # accepts no events
      end
    end
  end
end
