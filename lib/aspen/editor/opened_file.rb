# frozen_string_literal: true

module Aspen
  class Editor
    # Represents a file (name and its contents) that might not yet exist on the
    # filesystem. Instances of OpenedFile represent files that have been
    # specified by the user, such as on the command line, or via Editor#save_as.
    # All filesystem I/O should be funneled through this class so that it can
    # encapsulate the messiness of I/O and all of its errors.
    class OpenedFile
      def initialize(filename)
        @filename = filename
        @contents =
          begin
            File.read(filename)
          rescue Errno::ENOENT
            nil
          end
        @new = !@contents
      end

      attr_reader :filename, :contents

      def new? = @new

      def to_s
        "Opened #{filename}#{' (new file)' if new?}"
      end

      def save(contents)
        File.write(@filename, contents)
        @contents = contents
      end
    end
  end
end
