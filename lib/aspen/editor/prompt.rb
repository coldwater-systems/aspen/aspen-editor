# frozen_string_literal: true

require_relative '../output_block'

module Aspen
  class Editor
    # The line below the StatusLine that will take user input for commands such
    # as "Save As" or a command prompt.
    class Prompt
      def initialize(context:)
        @buffer = Buffer.new(context:)
        @prompt_string = ''
        @result_proc = nil
      end

      def output(size)
        raise ArgumentError, 'Height must be 1' if size.height != 1

        output = OutputBlock.new(size)
        output.print(@prompt_string)
        output.copy(@buffer.output(Size.new(size.width - @prompt_string.length, 1)),
                    output.position, include_cursor: true)

        output
      end

      def get(prompt_string, &result_proc)
        @prompt_string = prompt_string
        @result_proc = result_proc
      end

      def on_mouse_input(mouse)
        mouse = mouse.dup
        mouse.x -= @prompt_string.length
        @buffer.on_mouse_input(mouse)
      end

      def self.allow_from_keymap?(message)
        [
          :insert,
          :delete_left, :delete_right, :delete_selection,
          :move_left, :move_right, :extend_selection_left, :extend_selection_right,
          :move_to_line_start, :move_to_line_end,
          :finish, :cancel
        ].include?(message)
      end

      def insert(str) = @buffer.insert(str)
      def delete_left = @buffer.delete_left
      def delete_right = @buffer.delete_right
      def delete_selection = @buffer.delete_selection
      def move_left = @buffer.move_left
      def move_right = @buffer.move_right
      def extend_selection_left = @buffer.extend_selection_left
      def extend_selection_right = @buffer.extend_selection_right
      def move_to_line_start = @buffer.move_to_line_start
      def move_to_line_end = @buffer.move_to_line_end

      def finish
        return unless @result_proc

        @result_proc.call(@buffer.to_s.chomp)
        @buffer.clear
        @prompt_string = ''
        @result_proc = nil
      end

      def cancel
        return unless @result_proc

        @result_proc.call(:cancel)
        @buffer.clear
        @prompt_string = ''
        @result_proc = nil
      end
    end
  end
end
