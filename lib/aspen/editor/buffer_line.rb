# frozen_string_literal: true

require 'stringio'

module Aspen
  class Editor
    # A Buffer's text data is composed of an array if BufferLines. This class is
    # only responsible for the text data itself, items in the gutter such as
    # line numbers are handled by Buffer instead.
    class BufferLine
      def initialize(content = '')
        raise ArgumentError, 'BufferLine content must not be nil' unless content

        @content = content.freeze
        @wrapped = nil
        @wrapped_for_width = nil
      end

      attr_reader :content

      def hash = @content.hash
      def ==(other) = other.respond_to(:to_str) && @content == other.to_str
      alias eql? ==
      alias to_s content
      alias to_str content

      def [](...) = @content.[](...)
      def length = @content.length

      def inserted(col, content)
        BufferLine.new(@content[...col] << content << @content[col..])
      end

      def deleted_at(col)
        BufferLine.new(@content[...col] << @content[(col + 1)..])
      end

      def split_at(col)
        [BufferLine.new(@content[...col]), BufferLine.new(@content[col..])]
      end

      def +(other)
        BufferLine.new(@content + other.to_str)
      end

      def wrapped_height(width)
        rewrap(width) if width != @wrapped_for_width

        @wrapped.length
      end

      def wrapped(width)
        rewrap(width) if width != @wrapped_for_width

        @wrapped
      end

      private

      def rewrap(width)
        width = [1, width].max

        # TODO: need to figure fullwidth character support
        @wrapped = []
        col = 0
        while (slice = @content[col...col + width])
          @wrapped << slice
          col += width
        end
        @wrapped_for_width = width
      end
    end
  end
end
