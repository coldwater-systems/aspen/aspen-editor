# frozen_string_literal: true

require_relative '../output_block'
require_relative '../pair'
require_relative 'buffer_line'

module Aspen
  class Editor
    # A representation of textual data (one or more lines), and is typically
    # used for file contents.
    #
    # See spec/aspen/editor/coordinates.md for a glossary of coordinate
    # terminology used in this implementation.
    class Buffer
      def initialize(opened_file = nil, context:)
        @opened_file = opened_file
        @responder = context.responder
        @preferences = context.preferences.view('buffer')

        @lines = []
        @lines = opened_file.contents.lines(chomp: true).map(&BufferLine.method(:new)) if opened_file&.contents
        @lines << BufferLine.new if @lines.empty?

        @selection = BufferSelection.new(0, 0, lines:)
        @logical_scroll_y = 0
        @show_gutter = false
      end

      def filename = @opened_file&.filename

      attr_reader :lines, :selection, :width, :height
      attr_writer :show_gutter
      attr_accessor :opened_file

      def show_gutter? = @show_gutter
      def size = Size.new(@width, @height)
      def to_s = @lines.map {|line| "#{line}\n" }.join

      # These are in insertion point coordinates, not screen coordinates
      def insertion_point = BufferPosition.new(col, row)
      def row = selection.row
      def col = selection.col

      def output(size)
        @width, @height = Pair(size)
        return if width < gutter_width + 1

        output = OutputBlock.new(size)
        # XXX: this conditional is outside the cursor_position because we're naively using it in ensure_cursor_in_view
        output.cursor = cursor_position if cursor_position && focused? && cursor_position.y < height

        lines[@logical_scroll_y..].each_with_index do |line, line_index|
          row = @logical_scroll_y + line_index
          col_start = 0
          wrapped_lines = line.wrapped(width - gutter_width - 1)
          wrapped_lines.each_with_index do |display_line, display_line_index|
            return output if output.position.y >= height

            add_gutter_to(output, row, include_line_number: display_line_index == 0) if show_gutter?
            display_line.each_char.with_index do |char, char_index|
              add_character_to(output, char, row, col_start + char_index)
            end
            col_start += display_line.length
            if display_line_index < wrapped_lines.length - 1
              output.print '>', :wrap_symbol
            else
              add_character_to(output, "\n", row, col_start)
            end
            output.puts
          end
        end
        output
      end

      def add_gutter_to(output, row, include_line_number:)
        if include_line_number
          output.print "#{(row + 1).to_s.rjust(gutter_width - 2)}  ", :line_number
        else
          output.print ' ' * gutter_width, :line_number
        end
      end

      def add_character_to(output, char, row, col)
        sel_start, sel_end = selection.bounds
        selected = (row == sel_start.row && row != sel_end.row && col >= sel_start.col) ||
                   (row > sel_start.row && row < sel_end.row) ||
                   (row != sel_start.row && row == sel_end.row && col < sel_end.col) ||
                   (row == sel_start.row && row == sel_end.row && col >= sel_start.col && col < sel_end.col)

        if selected
          if char == ' '
            output.print '·', :selected_space
          else
            char = ' ' if char == "\n"
            output.print char, :selected
          end
        else
          output.print char unless char == "\n"
        end
      end

      def cursor_position
        return nil if row < @logical_scroll_y

        y_base = lines[@logical_scroll_y...row].map {|line| line.wrapped_height(width - gutter_width - 1) }.sum
        y_wrap, x = col.divmod(width - gutter_width - 1)
        if x == 0 && y_wrap > 0
          x = width - gutter_width - 1
          y_wrap -= 1
        end
        Position.new(gutter_width + x, y_base + y_wrap)
      end

      def gutter_width = show_gutter? ? 6 : 0
      def focused? = @responder.first == self
      def current_line = selection.current_line
      def status = selection.to_s

      def clear
        @lines.clear
        @lines << BufferLine.new
      end

      def on_mouse_input(mouse)
        case mouse.event
        when :down
          selection.move_to(mouse_to_buffer_position(mouse))
        when :drag
          selection.extend_to(mouse_to_buffer_position(mouse))
        when :scroll
          case mouse.button
          when :scroll_up
            # TODO: preferences should have a schema to enforce this type
            scroll_up(Integer(@preferences['mouse_scroll_sensitivity']))
          when :scroll_down
            scroll_down(Integer(@preferences['mouse_scroll_sensitivity']))
          end
        end
      end

      def mouse_to_buffer_position(mouse)
        y_remaining = mouse.y
        lines[@logical_scroll_y..].each_with_index do |line, line_index|
          row = @logical_scroll_y + line_index
          wrapped_lines = line.wrapped(width - gutter_width - 1)
          if y_remaining < wrapped_lines.length
            x_clamped = (mouse.x - gutter_width).clamp(0, wrapped_lines[y_remaining].length)
            return BufferPosition.new(row, wrapped_lines[0...y_remaining].map(&:length).sum + x_clamped)
          end
          y_remaining -= wrapped_lines.length
        end
        BufferPosition.new(@lines.length - 1, @lines[-1].length)
      end

      def save
        @opened_file.save(to_s)
      end

      # Keymap commands ################################################################################################

      def self.allow_from_keymap?(message)
        [
          :insert,
          :delete_left, :delete_right, :delete_selection,
          :move_up, :move_down, :move_left, :move_right,
          :extend_selection_up, :extend_selection_down, :extend_selection_left, :extend_selection_right,
          :move_to_top, :move_to_bottom, :move_to_line_start, :move_to_line_end,
          :scroll_down, :scroll_up
        ].include?(message)
      end

      def insert(str)
        delete_selection unless selection.empty?

        str.each_line do |insert_line|
          selection.clear_virtual_col
          lines[row] = current_line.inserted(col, insert_line.chomp)
          selection.col += insert_line.chomp.length
          selection.clear
          break unless insert_line.end_with?("\n")

          lines[row..row] = current_line.split_at(col)
          selection.move_right # off the end of the line onto the next line
        end
        ensure_cursor_in_view
        @show_logo = false
      end

      def delete_left
        return delete_selection unless selection.empty?

        if col > 0
          selection.move_left
          lines[row] = current_line.deleted_at(col)
        elsif row > 0
          selection.move_left
          lines[row] = current_line + lines.delete_at(row + 1)
        end
        ensure_cursor_in_view
      end

      def delete_right
        return delete_selection unless selection.empty?

        if col < current_line.length
          lines[row] = current_line.deleted_at(col)
        elsif row + 1 < lines.length
          lines[row] = current_line + lines.delete_at(row + 1)
        end
        ensure_cursor_in_view
      end

      def delete_selection
        # sel.row and sel.col will decrease as this method deletes from end to start
        sel_start, sel = selection.bounds

        while sel.row > sel_start.row
          after_selection = lines[sel.row][sel.col..]
          lines.delete_at(sel.row)
          sel.row -= 1
          sel.col = lines[sel.row].length
          lines[sel.row] += after_selection
        end

        before_selection = lines[sel.row][...sel_start.col]
        after_selection = lines[sel.row][sel.col...]
        lines[sel.row] = BufferLine.new(before_selection + after_selection)

        selection.move_to(sel_start)
        ensure_cursor_in_view
      end

      def move_up
        selection.move_up
        ensure_cursor_in_view
      end

      def move_down
        selection.move_down
        ensure_cursor_in_view
      end

      def move_left
        selection.move_left
        ensure_cursor_in_view
      end

      def move_right
        selection.move_right
        ensure_cursor_in_view
      end

      def extend_selection_up
        selection.extend_up
        ensure_cursor_in_view
      end

      def extend_selection_down
        selection.extend_down
        ensure_cursor_in_view
      end

      def extend_selection_left
        selection.extend_left
        ensure_cursor_in_view
      end

      def extend_selection_right
        selection.extend_right
        ensure_cursor_in_view
      end

      def move_to_top
        selection.move_to_top
        ensure_cursor_in_view
      end

      def move_to_bottom
        selection.move_to_bottom
        ensure_cursor_in_view
      end

      def move_to_line_start
        selection.move_to_line_start
        ensure_cursor_in_view
      end

      def move_to_line_end
        selection.move_to_line_end
        ensure_cursor_in_view
      end

      def scroll_up(by = 1)
        @logical_scroll_y = [@logical_scroll_y - by, 0].max
      end

      def scroll_down(by = 1)
        @logical_scroll_y = [@logical_scroll_y + by, lines.length - 1].min
      end

      def ensure_cursor_in_view
        @logical_scroll_y = row if row < @logical_scroll_y
        @logical_scroll_y += 1 while cursor_position.y > height - 1 # XXX: naive
      end
    end
  end
end
