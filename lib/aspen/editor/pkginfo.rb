# frozen_string_literal: true

module Aspen
  class Editor
    VERSION = '0.1.0'
    HOMEPAGE = 'https://gitlab.com/coldwater-systems/aspen/aspen-editor'
    LICENSE = 'GPL-3.0-only'

    VERSION_LONG = <<~VERSION_LONG.freeze
      Aspen Editor, version #{VERSION}
      Copyright (C) 2022  Alex Gittemeier
      #{HOMEPAGE}

      This program is free software: you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation, version 3 of the License.

      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

      See https://www.gnu.org/licenses/gpl-3.0.txt
    VERSION_LONG
  end
end
