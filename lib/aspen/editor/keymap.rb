# frozen_string_literal: true

require 'yaml'

module Aspen
  class Editor
    # A conversion from keystrokes/text into messages to send to other
    # components of the system. Every user input goes through this module so
    # that any input can be configured to do any action.
    class Keymap
      def initialize(keymap_filename)
        @keymap = {}
        yml_keymap = File.open(keymap_filename, &YAML.method(:safe_load))
        yml_keymap.each do |scope_spec, yml_keymap_in_scope|
          scope_spec.split(/\s*\|\s*/).each do |scope_str|
            scope = SCOPES.fetch(scope_str.to_sym)
            @keymap[scope] ||= {}

            yml_keymap_in_scope.each do |key_spec, (message_str, *params)|
              message = message_str.to_sym
              raise "Not allowed to call #{scope}.#{message} from keymap" unless scope.allow_from_keymap?(message)

              key_spec.split(/\s*\|\s*/).each do |key_str|
                @keymap[scope][Key.parse(key_str)] = [message, *params]
              end
            end
          end
        end
      end

      def action(input, chain)
        chain.lazy.filter_map do |responder|
          found = action_in_scope(input, responder.class)
          [responder, *found] if found
        end.first
      end

      def action_in_scope(input, scope)
        case input
        when Key
          @keymap.dig(scope, input)
        when String
          [:insert, input] if scope.allow_from_keymap?(:insert)
        end
      end
    end
  end
end
