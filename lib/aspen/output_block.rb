# frozen_string_literal: true

require_relative 'pair'
require_relative 'refinements'

module Aspen
  # A data structure representing the output of a component. #print and #puts
  # provide a text-like drawing interface, and these commands can be tagged. At
  # a later stage (currently in Aspen::Terminal), these tags will be mapped to
  # color values.
  class OutputBlock
    include Enumerable
    using EnumerableGroupBy

    def initialize(size)
      @size = Size(size)
      @position = Position.new(0, 0)
      @cursor = nil

      # @text and @tags are addressed as [y][x]
      @text = rectangular_array(@size, ' ')
      @tags = rectangular_array(@size, nil)
    end

    attr_reader :position, :cursor, :size

    def width = @size.width
    def height = @size.height

    def each
      return enum_for(:each) unless block_given?

      (0...height).each do |y|
        @text[y].group_by(@tags[y]) do |characters, tag|
          yield characters.join, tag
        end
        yield "\n", nil
      end
    end

    def puts(str = '', tag = :text)
      str += "\n" unless str.end_with?("\n")
      print(str, tag)
    end

    def print(str, tag = :text)
      str.each_grapheme_cluster do |character|
        if character == "\n"
          @position.x = 0
          @position.y += 1
        else
          set(@position, character, tag)
          @position.x += 1
        end
      end
    end

    def set(position, character, tag = :text)
      raise ArgumentError, "X must be (0...#{width}) but got #{position.x}" if position.x < 0 || position.x >= width
      raise ArgumentError, "Y must be (0...#{height}) but got #{position.y}" if position.y < 0 || position.y >= height
      raise ArgumentError, "#{character} is not a single grapheme" if character.grapheme_clusters.length != 1

      add_alignment_tag_up_to(position.x - 1, position.y)
      @text[position.y][position.x] = character
      @tags[position.y][position.x] = tag
    end

    def copy(other, position, include_cursor: false)
      if include_cursor && other.cursor
        raise 'Another component already set a cursor' if cursor

        self.cursor = position + other.cursor
      end

      (0...other.height).each do |y|
        add_alignment_tag_up_to(position.x - 1, position.y + y)
        @text[position.y + y][position.x...(position.x + other.width)] = other.text[y][0...other.width]
        @tags[position.y + y][position.x...(position.x + other.width)] = other.tags[y][0...other.width]
      end
    end

    def cursor=(cursor)
      cursor &&= Position(cursor)
      if cursor
        raise RangeError, "X must be (0...#{width}) but got #{cursor.x}" if cursor.x < 0 || cursor.x >= width
        raise RangeError, "Y must be (0...#{height}) but got #{cursor.y}" if cursor.y < 0 || cursor.y >= height
      end

      @cursor = cursor
    end

    def cursor_at_position
      self.cursor = @position
    end

    protected

    attr_reader :text, :tags

    private

    def rectangular_array(size, fill)
      (0...size.height).map { ([fill] * size.width) }
    end

    def add_alignment_tag_up_to(x, y)
      while x >= 0 && !@tags[y][x]
        @tags[y][x] = :alignment
        x -= 1
      end
    end
  end
end
