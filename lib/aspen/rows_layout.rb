# frozen_string_literal: true

module Aspen
  # A component that has one or more children laid out in rows. Each child
  # component is sized at the full width, and the height is calculated Based on
  # the values of `height` and `grow`, which control the initial height and
  # if/how much they should grow to fill unfilled space, respectively.
  # TODO: come up with a replacement for #add that makes the callsite look like
  # the eventual hierarchy (like a UI lang like html/haml does)
  class RowsLayout
    Child = Struct.new(:component, :height, :grow, keyword_init: true)

    def initialize
      @children = []
      @calculated_heights = nil
      @calculated_heights_for_total_height = nil
      @size = nil
    end

    attr_reader :children

    def add(component, height: 0, grow: 0)
      raise ArgumentError, 'Must specify non-zero height and/or grow' if height == 0 && grow == 0
      raise ArgumentError, 'height must not be negative' if height < 0
      raise ArgumentError, 'grow must not be negative' if grow < 0

      @children << Child.new(component:, height:, grow:)
      component
    end

    def output(size)
      @size = size
      output = OutputBlock.new(size)

      components_with_height(size.height).each do |(child, child_height)|
        output.copy(
          child.component.output(Size.new(size.width, child_height)),
          Position.new(0, output.position.y),
          include_cursor: true
        )
        output.position.y += child_height
      end
      output
    end

    def on_mouse_input(mouse)
      translated_mouse = mouse.dup
      components_with_height(@size.height).each do |(child, child_height)|
        return child.component.on_mouse_input(translated_mouse) if translated_mouse.y < child_height

        translated_mouse.y -= child_height
      end
      raise
    end

    def components_with_height(total_height)
      recalculate_heights(total_height) if total_height != @calculated_heights_for_total_height

      @children.zip(@calculated_heights)
    end

    private ####################################################################

    def recalculate_heights(total_height)
      total_grow = @children.map(&:grow).sum
      @calculated_heights =
        if total_grow > 0
          unfilled_height = total_height - @children.map(&:height).sum

          @children.map do |child|
            # TODO: this has a rounding error if there is more than one child with a `grow`
            child.height + (unfilled_height * child.grow / total_grow)
          end
        else
          children.map(&:height)
        end
    end
  end
end
