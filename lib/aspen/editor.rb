# frozen_string_literal: true

require_relative 'editor/buffer'
require_relative 'editor/buffer_line'
require_relative 'editor/buffer_selection'
require_relative 'editor/context'
require_relative 'editor/keymap'
require_relative 'editor/opened_file'
require_relative 'editor/pkginfo'
require_relative 'editor/preferences'
require_relative 'editor/prompt'
require_relative 'editor/status_line'
require_relative 'responder'
require_relative 'rows_layout'
require_relative 'terminal'
require_relative 'terminal_parser'

module Aspen
  # Entry point for the Aspen's CLI editor. This class defines the relationships
  # between the other components of the editor.
  class Editor
    SCOPES = {
      editor: Editor,
      buffer: Buffer,
      prompt: Prompt,
    }.freeze

    HELP_TEXT = 'Ctrl-S to save, Ctrl-W to save and quit, Ctrl-Alt-W to quit without saving'

    def initialize(filename = nil)
      @filename = filename
      @preferences = Preferences.new(File.expand_path('editor/preferences.yml', __dir__))
      @keymap = Keymap.new(File.expand_path('editor/keymap.yml', __dir__))
      @responder = Responder.new
      context = Editor::Context.new(
        responder:   @responder,
        preferences: @preferences
      )

      @terminal = Terminal.new($stdin, $stdout, preferences: @preferences)
      @terminal_parser = TerminalParser.new
      @root_layout = RowsLayout.new

      opened_file = @filename && OpenedFile.new(@filename)
      @buffer = @root_layout.add(Buffer.new(*opened_file, context:), grow: 1)
      @buffer.show_gutter = true
      activate(@buffer)

      @status_line = @root_layout.add(StatusLine.new, height: 1)
      @status_line.active_buffer = @buffer
      @status_line.status = @buffer.opened_file ? @buffer.opened_file.to_s : HELP_TEXT

      @prompt = @root_layout.add(Prompt.new(context:), height: 1)
    end

    def run
      @terminal.raw(:alt_screen, :mouse_tracking) do # TODO: add :bracketed_paste
        loop do
          @terminal.print_output(@root_layout)

          @status_line.status = HELP_TEXT
          @terminal_parser.parse(@terminal.readpartial(4096)) do |input|
            if input.is_a?(MouseInput)
              @root_layout.on_mouse_input(input)
            else
              action = @keymap.action(input, @responder.chain)
              @responder.send_action(*action) if action
            end
          end
        end
      end
    end

    # Keymap commands ##################################################################################################

    def self.allow_from_keymap?(message)
      [:save, :save_as, :save_and_close, :close_without_prompt].include?(message)
    end

    def save(and_then: nil)
      if @buffer.filename
        @buffer.save
        @status_line.status = "Saved to #{@filename}"
        and_then&.call
      else
        save_as(and_then:)
      end
    end

    def save_as(and_then: nil)
      activate(@prompt)
      @prompt.get('Save as: ') do |filename|
        activate(@buffer)
        next if filename == :cancel

        filename.strip!
        next if filename.empty?

        @buffer.opened_file = OpenedFile.new(filename)
        save(and_then:)
      end
    end

    def save_and_close
      save(and_then: method(:close_without_prompt))
    end

    def close_without_prompt
      raise StopIteration
    end

    private ############################################################################################################

    def activate(*)
      @responder.activate(*, self)
    end
  end
end
