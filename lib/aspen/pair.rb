# frozen_string_literal: true

# rubocop:disable Style/Documentation - Kernel is already documented
# rubocop:disable Naming/MethodName - we're adding converting helper methods
module Kernel
  def Pair(pair_like)
    pair = Array(pair_like)
    raise ArgumentError, "Expected 2-element array but got #{pair_like.inspect}" if pair.length != 2

    pair.freeze
  end

  def Position(pair_like) = pair_like.is_a?(Position) ? pair_like : Position.new(*Pair(pair_like))
  def Size(pair_like) = pair_like.is_a?(Size) ? pair_like : Size.new(*Pair(pair_like))
  def BufferPosition(pair_like) = pair_like.is_a?(BufferPosition) ? pair_like : BufferPosition.new(*Pair(pair_like))
end
# rubocop:enable Style/Documentation
# rubocop:enable Naming/MethodName

Position = Struct.new(:x, :y) do
  def inspect = "Position(#{x}, #{y})"
  def +(other) = Position.new(x + other.x, y + other.y)
end

Size = Struct.new(:width, :height) do
  def inspect = "Size(#{width}, #{height})"
end

BufferPosition = Struct.new(:row, :col) do
  def inspect = "BufferPosition(#{row}, #{col})"
end
