# frozen_string_literal: true

module Aspen
  # A keystroke other than those that produce text. A keystrokes that do produce
  # text are represented by Strings instead
  class Key
    # TODO: move some of this validation into the constructor
    def self.parse(str)
      *modifiers, name = str.downcase.split('+')
      modifiers = modifiers.map(&:to_sym)
      raise ArgumentError, "#{str.inspect} has invalid modifiers" if modifiers.any? {|m| !MODIFIERS.include?(m) }

      if KEYS_REQUIRING_MODIFIERS.include?(name.upcase)
        raise ArgumentError, "#{str.inspect} #{IS_NOT_A_KEY}" if modifiers.empty? || modifiers == [:shift]

        new(modifiers, name.upcase)
      elsif SPECIAL_KEYS.include?(name.to_sym)
        raise ArgumentError, "#{str.inspect} #{IS_NOT_A_KEY}" if modifiers.empty? && name == 'space'

        new(modifiers, name.to_sym)
      else
        raise ArgumentError, "#{str.inspect}: final key name not recognized"
      end
    end

    # TODO: rename mods to modifiers
    # TODO: see if we can splat the first argument
    def initialize(mods, name)
      @mods = Set.new(mods) unless mods.is_a?(Set)
      @name = name
    end

    attr_reader :mods, :name

    def to_s
      mod_keys = mods.sort_by {|mod| MODIFIERS.index(mod) || mod }
      (mod_keys + [name]).map(&:to_s).join('+')
    end

    def inspect
      "#<#{self.class} #{self}>"
    end

    def ==(other)
      return false unless other.is_a?(Key)

      mods == other.mods && name == other.name
    end
    alias eql? ==

    def hash
      [mods, name].hash
    end

    # private ##########################################################################################################

    MODIFIERS = [:ctrl, :alt, :shift].freeze
    SPECIAL_KEYS = Set.new([
      :f1, :f2, :f3, :f4, :f5, :f6, :f7, :f8, :f9, :f10, :f11, :f12,
      :esc, :backspace, :tab, :enter, :space,
      :insert, :home, :page_up, :delete, :end, :page_down,
      :up, :left, :down, :right
    ])
    KEYS_REQUIRING_MODIFIERS = Set.new("`1234567890-=QWERTYUIOP[]\\ASDFGHJKL;'ZXCVBNM,./".chars)
    IS_NOT_A_KEY = 'is not a Aspen::Key because it produces text. Use the string directly instead?'
  end
end
