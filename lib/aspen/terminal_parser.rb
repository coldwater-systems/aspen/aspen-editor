# frozen_string_literal: true

require 'strscan'
require_relative 'key'
require_relative 'mouse_input'

module Aspen
  # Parses EMCA-48:1991/UTF-8 encoded input from a tty, supporting xterm-like
  # terminals, and the local linux terminal.
  #
  # TODO: make sure we're getting octets from the terminal and THEN coding to
  # UTF-8, not UTF-8 input and then gleaning sequences from that.
  #
  # TODO: investigate the effect of DECSET 1035 on metakeys (or DECSET ?1)
  #
  # The following documents or standards were used to construct this parser:
  #
  # - ECMA-48: especially its definition of C0/C1 control codes,
  #   of which we use ESC `^[`, and CSI `^[[` (and many of its sequences)
  #   - https://en.wikipedia.org/wiki/ANSI_escape_code
  #   - https://www.ecma-international.org/publications-and-standards/standards/ecma-48/
  # - vt510: VTxxx programmer's manual
  #   - https://www.vt100.net/docs/vt510-rm/contents.html
  # - xterm: https://invisible-island.net/xterm/ctlseqs/ctlseqs.html
  #   - This document explains why direct C1 control sequences are not used in
  #     UTF-8 (they don't save any space in any event).
  # - CSI u: A loose specification of CSI ...;... u sequences (and other modifier extensions)
  #   - http://www.leonerd.org.uk/hacks/fixterms/
  #     - limitations of this are discussed in https://sw.kovidgoyal.net/kitty/keyboard-protocol/
  # - spec/terminals/*.yml: A record of sequences of actual terminals this
  #   parser supports
  class TerminalParser
    def initialize
      reset_buffer
    end

    def eos? = @buffer.eos?

    def inspect
      "#<#{self.class} buffer=#{@buffer.rest.inspect}>"
    end

    def <<(raw_input) = @buffer << raw_input

    def parse(raw_input = nil)
      return enum_for(:parse, raw_input) unless block_given?

      self << raw_input if raw_input
      while (input = next_input)
        yield input
      end
      self
    end

    def next_input
      token = next_token
      return nil unless token

      token_class, matched, captures = token
      case token_class
      when :text
        matched
      when :key
        unicode_to_key([], matched)
      when :meta_key
        unicode_to_key([:alt], *captures)
      when :csi # short for control sequence introducer (ECMA-48)
        csi_to_key([], *captures)
      when :ss3 # some xterm keys are SS3 but they're identical to their CSI counterparts
        csi_to_key([], '', *captures)
      when :meta_csi
        csi_to_key([:alt], *captures)
      else
        raise NotImplementedError, token
      end
    end

    private ############################################################################################################

    def reset_buffer = @buffer = StringScanner.new(String.new)

    def next_token
      [
        # Ambiguous sequences that could be part of something larger. We use the
        # end of string to disambiguate
        [:key, /\e\z/], # Could have been the start of a SS3, CSI, or ALT+... code
        [:meta_key, /\e([\e\[O])\z/], # Could have been the start of a SS3 or CSI sequence

        # Unambiguous sequences
        [:csi, /\e\[([\x30-\x3F]*)([\x20-\x2F]*[\x40-\x7E])/], # e.g. ^[[1;5H or ^[[A
        [:ss3, /\eO([\x00-\x7F])/], # e.g. ^[OP
        [:meta_csi, /\e\e\[([\x30-\x3F]*)([\x20-\x2F]*[\x40-\x7E])/], # e.g. ^[^[[6~
        [:key, /[\x00-\x1A\x1C-\x1F\x7F\uF746]/],
        [:meta_key, /\e([^\e\[O])/],

        # Base case
        [:text, /[^\e]/],
      ].each do |(token_class, regex)|
        matched = @buffer.scan(regex)
        next unless matched

        captures = @buffer.captures
        reset_buffer if @buffer.eos?
        return [token_class, matched, captures]
      end
      nil
    end

    def csi_to_key(extra_modifiers, parameter_str, identifier)
      case identifier
      when *DIRECT_CSI_KEYS.keys
        csi_direct_to_key(extra_modifiers, parameter_str, DIRECT_CSI_KEYS.fetch(identifier))
      when 'u'
        codepoint, modifier_mask = csi_parse_numeric(parameter_str, 2)
        unicode_to_key(csi_modifiers(modifier_mask), String.new << codepoint)
      when '~' # DECFNK - Function key (vt510) [cursor/editing keys sometimes send these too]
        csi_lookup_to_key(extra_modifiers, parameter_str, FUNCTION_KEYS)
      when 'M', 'm' # Mouse Tracking (xterm) in SGR extended mode (DECSET 1006)
        raise NotImplementedError, [:csi, parameter_str, identifier].inspect if parameter_str[0] != '<'

        csi_mouse_sgr_to_mouse(extra_modifiers, parameter_str[1..], identifier)
      else
        raise NotImplementedError, [:csi, parameter_str, identifier].inspect
      end
    end

    def unicode_to_key(extra_modifiers, character)
      if SPECIAL_CHARACTERS.key?(character)
        *modifiers, name = SPECIAL_CHARACTERS[character]
        Key.new(extra_modifiers + modifiers, name)
      elsif character.ord < 32 # C0 characters not in SPECIAL_CHARACTERS are ^A, ^B, etc.
        unicode_to_key(extra_modifiers + [:ctrl], (character.ord + 64).chr.downcase)
      elsif ('A'..'Z').include?(character)
        Key.new(extra_modifiers + [:shift], character)
      else
        Key.new(extra_modifiers, character.upcase)
      end
    end

    def csi_direct_to_key(extra_modifiers, parameter_str, name)
      # Keys like "up" are usually simple like ^[[A, but if they have modifiers
      # implies ^[[1A so that, e.g. shift+up can be ^[[1;2A (CSI u)
      csi_lookup_to_key(extra_modifiers, parameter_str, {1 => name})
    end

    def csi_lookup_to_key(extra_modifiers, parameter_str, lookup)
      function_id, modifier_mask = csi_parse_numeric(parameter_str, 2)
      *implicit_modifiers, name = lookup.fetch(function_id)
      Key.new(extra_modifiers + csi_modifiers(modifier_mask) + implicit_modifiers, name)
    end

    def csi_mouse_sgr_to_mouse(extra_modifiers, parameter_str, identifier)
      modifiers = extra_modifiers.dup
      event_code, x, y = csi_parse_numeric(parameter_str, 3)

      button_hi =            (event_code & 0b11000000) >> 6
      motion =               (event_code & 0b00100000) != 0
      modifiers << :ctrl  if (event_code & 0b00010000) != 0
      modifiers << :alt   if (event_code & 0b00001000) != 0
      modifiers << :shift if (event_code & 0b00000100) != 0
      button_lo =            (event_code & 0b00000011)

      button = button_hi * 4 + button_lo
      button = MOUSE_BUTTONS[button] if button < MOUSE_BUTTONS.length

      event =
        if motion && button
          :drag
        elsif motion
          :move
        elsif MOUSE_SCROLL_BUTTONS.include?(button)
          :scroll
        elsif identifier == 'M'
          :down
        elsif identifier == 'm'
          :up
        else
          raise
        end
      MouseInput.new(event, modifiers, button, x - 1, y - 1)
    end

    def csi_parse_numeric(parameter_str, expected_count)
      parameters = parameter_str.split(';').map(&method(:Integer))
      parameters << 1 while parameters.length < expected_count
      raise ArgumentError, "More parameters in #{parameter_str} than expected" if parameters.length != expected_count

      parameters
    end

    ## Tables from various standards into standard semantics ###########################################################

    # vt510 table 5-5: DECFNK Modifiers in effect
    # extended by xterm "PC-Style Function Keys"
    def csi_modifiers(modifier_mask)
      mods = []
      mods << :ctrl  if (modifier_mask - 1) & 0b0100 != 0
      mods << :alt   if (modifier_mask - 1) & 0b1010 != 0 # both xterm-meta and vt510-alt should be :alt
      mods << :shift if (modifier_mask - 1) & 0b0001 != 0
      mods
    end

    DIRECT_CSI_KEYS = {
      'A' => [:up],          # CUU - Cursor Up (vt510 chapter 5)
      'B' => [:down],        # CUD - Cursor Down (vt510 chapter 5)
      'C' => [:right],       # CUF - Cursor Forward (vt510 chapter 5)
      'D' => [:left],        # CUB - Cursor Backward (vt510 chapter 5)
      'F' => [:end],         # (xterm)
      'H' => [:home],        # (xterm)
      'P' => [:f1],          # [citation needed]
      'Q' => [:f2],          # [citation needed]
      'R' => [:f3],          # [citation needed]
      'S' => [:f4],          # [citation needed]
      'Z' => [:shift, :tab], # CBT - Cursor Backward Tabulation (vt510 chapter 5)
    }.freeze

    # TODO: Have not found a citation for any of these yet, the ctrl+insert is
    # the most interesting of them all
    SPECIAL_CHARACTERS = {
      "\e"     => [:esc],
      '~'      => [:shift, '`'],
      '!'      => [:shift, '1'],
      '@'      => [:shift, '2'],
      '#'      => [:shift, '3'],
      '$'      => [:shift, '4'],
      '%'      => [:shift, '5'],
      '^'      => [:shift, '6'],
      '&'      => [:shift, '7'],
      '*'      => [:shift, '8'],
      '('      => [:shift, '9'],
      ')'      => [:shift, '0'],
      '_'      => [:shift, '-'],
      '+'      => [:shift, '='],
      "\x7F"   => [:backspace],
      "\t"     => [:tab],
      '{'      => [:shift, '['],
      '}'      => [:shift, ']'],
      '|'      => [:shift, '\\'],
      ':'      => [:shift, ';'],
      '"'      => [:shift, "'"],
      "\r"     => [:enter],
      '<'      => [:shift, ','],
      '>'      => [:shift, '.'],
      '?'      => [:shift, '/'],
      ' '      => [:space],
      "\0"     => [:ctrl, :space],
      "\uF746" => [:ctrl, :insert],
    }.freeze

    # vt510 table 5-5: DECFNK Function keystrokes
    FUNCTION_KEYS = {
      # (vt510 table 8-2: "Editing Keypad Sequences for PC Layout" in VT style)
      1 => [:home], 2 => [:insert], 3 => [:delete], 4 => [:end], 5 => [:page_up], 6 => [:page_down],
      # TODO: These have not been attested in an actual term yet!
      # 7 => [:right], 8 => [:down], 9 => [:up], 10 => [:left],
      # Skips in the numbering of Fn keys are intentional
      11 => [:f1],  12 => [:f2],  13 => [:f3],  14 => [:f4],  15 => [:f5],
      17 => [:f6],  18 => [:f7],  19 => [:f8],  20 => [:f9],  21 => [:f10],
      23 => [:f11], 24 => [:f12]
      # TODO: These have not been attested in an actual term yet (and they
      # conflict with the linux terminal)
      # 25 => [:f13], 26 => [:f14],
      # 28 => [:f15], 29 => [:f16],
      # 31 => [:f17], 32 => [:f18], 33 => [:f19], 34 => [:f20]
    }.freeze

    # xterm Mouse Tracking, Normal Tracking Mode
    MOUSE_BUTTONS = [
      :left,
      :middle,
      :right,
      nil, # release
      :scroll_up,
      :scroll_down,
      :scroll_left, # Not attested, and only implied in xterm reference docs
      :scroll_right, # ditto
    ].freeze

    # the subset above to be interpreted as a scroll event
    MOUSE_SCROLL_BUTTONS = Set.new([:scroll_up, :scroll_down, :scroll_left, :scroll_right])
  end
end
