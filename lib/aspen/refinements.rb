# frozen_string_literal: true

module Aspen
  # Provide #group_by to enumerables
  module EnumerableGroupBy
    refine Enumerable do
      # Zip `self` and `other`, then for each group (as defined by a run of
      # identical elements in `other`), yield the elements of `self` that
      # correspond to the group and the value of `other` that defines the group.
      # nil, :_separator, and :_alone in `other` are handled as Enumerable#chunk
      # would handle them.
      def group_by(other)
        return enum_for(:group_by, other) unless block_given?

        zip(other).chunk(&:last).each do |group, elements|
          yield elements.map(&:first), group
        end
      end
    end
  end
end
