# frozen_string_literal: true

require 'io/console'
require 'pastel'
require_relative 'pair'

module Aspen
  # Wraps stdin and stdout pointed at a tty, and extends the interface of IO to
  # include xterm specific setup/teardown
  class Terminal
    def initialize(stdin = $stdin, stdout = $stdout, preferences:)
      raise ArgumentError, 'The input must be a terminal' unless stdin.tty?
      raise ArgumentError, 'The output must be a terminal' unless stdout.tty?

      trap('WINCH') do
        # cause Errno::EINTR in #readpartial
      end

      @stdin = stdin
      @stdout = stdout
      @pastel = Pastel.new
      @pastel.alias_color(:default, :clear)

      @color_theme = preferences['color_theme'].to_h do |tag, color_spec|
        [tag.to_sym, color_spec.split(/\s+/).map(&:to_sym)]
      end
    end

    attr_reader :stdin, :stdout

    def raw(*)
      @stdin.raw do
        write(*features_on(*))
        yield
      ensure
        write(*features_off(*), FEATURES[:cursor].on)
      end
    end

    def readpartial(size)
      # TODO: we actually shouldn't be interpreting as UTF-8 at this stage (see TODO in TerminalParser)
      stdin.sysread(size).force_encoding('UTF-8')
    rescue Errno::EINTR
      nil
    end

    def print_output(component)
      output = component.output(size)
      output_raw = String.new

      output.each do |str, tag|
        if str == "\n"
          output_raw << "\r\n"
        else
          str = @pastel.decorate(str, *@color_theme.fetch(tag))
          output_raw << str
        end
      end

      write(
        csi_clear_screen, csi_goto(0, 0),
        output_raw.chomp,
        if output.cursor
          FEATURES[:cursor].on + csi_goto(*output.cursor)
        else
          FEATURES[:cursor].off
        end
      )
    end

    def size = Size($stdout.winsize.reverse)

    private ############################################################################################################

    # TODO: replace this with termcap/terminfo?
    def csi(*params, id) = "\e[#{params.join(';')}#{id}"
    def csi_clear_screen = csi(2, 'J')
    def csi_goto(col, row) = csi(row + 1, col + 1, 'H')
    def features_on(*features) = features.map {|sym| FEATURES.fetch(sym).on }
    def features_off(*features) = features.map {|sym| FEATURES.fetch(sym).off }

    def write(*str) = @stdout.write(str.join)

    Feature = Struct.new(:on, :off)
    FEATURES = {
      cursor:          Feature.new("\e[?25h", "\e[?25l"),
      # button event tracking (1002) and extended coordinates (1006)
      # TODO: change 1002 to 1003 if mouse motion is useful
      mouse_tracking:  Feature.new("\e[?1002h\e[?1006h", "\e[?1002l\e[?1006l"),
      alt_screen:      Feature.new("\e[?1049h", "\e[?1049l"),
      bracketed_paste: Feature.new("\e[?2004h", "\e[?2004l"),
    }.freeze
  end
end
