# frozen_string_literal: true

module Aspen
  # This dispatches keyboard events to the currently focused component.
  class Responder
    def initialize(*active_components)
      @active_components = active_components
    end

    def chain = @active_components
    def first = @active_components.first

    def activate(*components)
      @active_components = components
    end

    def send_action(action_target, action, *)
      action_target.public_send(action, *)
    end
  end
end
