# frozen_string_literal: true

module Aspen
  # A mouse event, whose event can be one of :drag, :move, :scroll, :up, or :down
  class MouseInput
    def initialize(event, modifiers, button, x, y)
      @event = event
      @modifiers = modifiers
      @button = button
      @x = x
      @y = y
    end

    attr_accessor :event, :modifiers, :button, :x, :y

    def to_s
      "#{event} #{[*modifiers, button].join('+')} (#{x}, #{y})"
    end

    def inspect
      "#<#{self.class} #{self}>"
    end
  end
end
