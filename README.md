# Aspen Editor

![](spec/aspen/editor/example.png)

## Setup

Make sure you have ruby 3.0 or later installed, and the bundler gem. Clone the
project, then run `bin/setup` from within the project directory.

## Usage

Either run `exe/edit` with no arguments for a blank editor, or run
`exe/edit "Some file.txt"` to edit an existing file.

If you would like to add `edit` to your path, run `rake install` within the
project directory.

## Development Notes

This project uses [rbs] signatures in the sig/ folder, which are checked during
rspec tests, but this will only catch issues exposed by test cases. You can run
`rake checksigs` to validate the syntax and check for referenced but undefined
types.

This project also is experimenting with the use of [steep] which consumes the
.rbs files and performs static type checking. The checks are quite strict and
this codebase takes advantage of the dynamic nature of Ruby, so there are lots
of errors. You can run the check via `steep check`.

[rbs]: https://github.com/ruby/rbs
[steep]: https://github.com/soutaro/steep
