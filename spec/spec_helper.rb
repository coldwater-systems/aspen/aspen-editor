# frozen_string_literal: true

require 'bundler/setup'
require 'simplecov'

require_relative '../sig/aspen/rbs_config'
ENV['RBS_TEST_OPT'] ||= Aspen::RBS_FLAGS
ENV['RBS_TEST_TARGET'] ||= 'Aspen::*'
ENV['RBS_TEST_LOGLEVEL'] ||= 'warn'
ENV['RBS_TEST_DOUBLE_SUITE'] ||= 'rspec'
ENV['RBS_TEST_UNCHECKED_CLASSES'] ||= '::RSpec::Mocks::Double,::RSpec::Mocks::InstanceVerifyingDouble'
require 'rbs/test/setup'

SimpleCov.start do
  enable_coverage :branch
  coverage_dir File.join(__dir__, 'coverage')
end

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true # default in RSpec 4
    expectations.syntax = :expect
  end
  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true # default in RSpec 4
  end
  config.shared_context_metadata_behavior = :apply_to_host_groups # default in Rspec 4

  # enable fit, fdescribe, fcontext
  config.filter_run_when_matching :focus

  # Enable flags like --only-failures and --next-failure
  # Should be enabled only if the entire suite takes too long
  # config.example_status_persistence_file_path = 'spec/.status'

  config.disable_monkey_patching!
  config.warnings = true

  # Print the 10 slowest examples and example groups at the
  # end of the spec run, to help surface which specs are running
  # particularly slow.
  # config.profile_examples = 3

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = :random
  Kernel.srand config.seed
end
