# frozen_string_literal: true

require 'aspen/editor'

RSpec.describe Aspen::Editor do
  it 'has a version number' do
    expect(Aspen::Editor::VERSION).not_to be_nil
  end
end
