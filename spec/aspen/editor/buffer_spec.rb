# frozen_string_literal: true

require 'aspen/editor/buffer'
require 'aspen/editor/context'
require 'aspen/editor/preferences'

RSpec.describe Aspen::Editor::Buffer do
  subject(:buffer) { described_class.new(context:) }

  let(:context) do
    Aspen::Editor::Context.new(
      responder:   mock_responder,
      preferences: Aspen::Editor::Preferences.new(File.expand_path('preferences.yml', __dir__))
    )
  end
  let(:mock_responder) { instance_double(Aspen::Responder) }
  let(:mock_opened_file) { instance_double(Aspen::Editor::OpenedFile) }
  let(:test_content) { "The quick brown fox jumps over the lazy dog.\n...\n\nAnd everyone loved it!" }

  let(:pastel) { Pastel.new(enabled: true) }
  let(:inverse) { pastel.lookup(:inverse) }
  let(:lineno_color) { pastel.lookup(:bright_black) }
  let(:reset) { pastel.lookup(:reset) }

  def expect_buffer_state_to_be(col, row, lines)
    expect(buffer.lines.map(&:to_s)).to eq(lines)
    expect(buffer.insertion_point).to eq(BufferPosition.new(col, row))
  end

  before do
    allow(mock_responder).to receive(:first).and_return(buffer)
    buffer.output(Size.new(80, 24))
  end

  it 'starts with an empty line' do
    expect_buffer_state_to_be(0, 0, [''])
  end

  describe 'insert' do
    specify { expect(described_class).to be_allow_from_keymap(:insert) }

    describe 'basic text entry' do
      example do
        buffer.insert "\n"
        expect_buffer_state_to_be(0, 1, ['', ''])
      end

      # Only \n is canonical, but be tolerant of CR LF also
      example do
        buffer.insert "\r\n"
        expect_buffer_state_to_be(0, 1, ['', ''])
      end

      example do
        buffer.insert 'abc'
        expect_buffer_state_to_be(3, 0, ['abc'])
      end

      example do
        'abc'.each_char(&buffer.method(:insert))
        expect_buffer_state_to_be(3, 0, ['abc'])
      end

      example do
        buffer.insert "line one\nline two"
        expect_buffer_state_to_be(8, 1, ['line one', 'line two'])
      end

      example do
        "line one\nline two".each_char(&buffer.method(:insert))
        expect_buffer_state_to_be(8, 1, ['line one', 'line two'])
      end
    end

    it 'inserts into middle of text' do
      buffer.insert 'place'
      2.times { buffer.move_left }
      buffer.insert 'net i'
      expect_buffer_state_to_be(8, 0, ['planet ice'])
    end

    it 'splits one line into two' do
      buffer.insert 'quartermaster'
      6.times { buffer.move_left }
      buffer.insert "\n"
      expect_buffer_state_to_be(0, 1, ['quarter', 'master'])
    end

    it 'handles the virtual column correctly' do
      buffer.insert "\ny"
      buffer.move_up
      buffer.insert 'x'
      expect_buffer_state_to_be(1, 0, ['x', 'y'])
    end
  end

  describe 'delete_left' do
    specify { expect(described_class).to be_allow_from_keymap(:delete_left) }

    it 'joins two lines' do
      buffer.insert "quarter\nmaster"
      6.times { buffer.move_left }
      buffer.delete_left
      expect_buffer_state_to_be(7, 0, ['quartermaster'])
    end

    it 'deletes characters at end of string' do
      buffer.insert 'hey'
      3.times { buffer.delete_left }
      expect_buffer_state_to_be(0, 0, [''])
    end

    it 'deletes characters in the middle of string' do
      buffer.insert 'adjoin'
      4.times { buffer.move_left }
      2.times { buffer.delete_left }
      expect_buffer_state_to_be(0, 0, ['join'])
    end

    it 'does nothing in empty buffer' do
      buffer.delete_left
      expect_buffer_state_to_be(0, 0, [''])
    end

    it 'does nothing at beginning of buffer' do
      buffer.insert 'hey'
      3.times { buffer.move_left }
      expect_buffer_state_to_be(0, 0, ['hey'])
      buffer.delete_left
      expect_buffer_state_to_be(0, 0, ['hey'])
    end
  end

  describe 'cursor behavior' do
    before do
      buffer.insert "hello there\n\nhi!"
      expect_buffer_state_to_be(3, 2, ['hello there', '', 'hi!'])
    end

    specify { expect(described_class).to be_allow_from_keymap(:move_down) }
    specify { expect(described_class).to be_allow_from_keymap(:move_up) }
    specify { expect(described_class).to be_allow_from_keymap(:move_left) }
    specify { expect(described_class).to be_allow_from_keymap(:move_right) }

    context 'when at beginning of string' do
      before do
        2.times { buffer.move_up }
        3.times { buffer.move_left }
        expect_buffer_state_to_be(0, 0, ['hello there', '', 'hi!'])
      end

      it 'does not move left' do
        buffer.move_left
        expect_buffer_state_to_be(0, 0, ['hello there', '', 'hi!'])
      end

      it 'does not move up' do
        buffer.move_up
        expect_buffer_state_to_be(0, 0, ['hello there', '', 'hi!'])
      end

      it 'moves down' do
        2.times { buffer.move_down }
        expect_buffer_state_to_be(0, 2, ['hello there', '', 'hi!'])
      end

      it 'moves down, staying on column even if passing through a blank line' do
        3.times { buffer.move_right }
        2.times { buffer.move_down }
        expect_buffer_state_to_be(3, 2, ['hello there', '', 'hi!'])
      end

      it 'moves right' do
        11.times { buffer.move_right }
        expect_buffer_state_to_be(11, 0, ['hello there', '', 'hi!'])
      end

      it 'moves right, wrapping across lines' do
        13.times { buffer.move_right }
        expect_buffer_state_to_be(0, 2, ['hello there', '', 'hi!'])
      end
    end

    context 'when at end of string' do
      it 'moves up, staying on column even if passing through a blank line' do
        2.times { buffer.move_up }
        expect_buffer_state_to_be(3, 0, ['hello there', '', 'hi!'])
      end

      it 'moves left' do
        3.times { buffer.move_left }
        expect_buffer_state_to_be(0, 2, ['hello there', '', 'hi!'])
      end

      it 'moves left, wrapping across lines' do
        5.times { buffer.move_left }
        expect_buffer_state_to_be(11, 0, ['hello there', '', 'hi!'])
      end

      it 'does not move right' do
        buffer.move_right
        expect_buffer_state_to_be(3, 2, ['hello there', '', 'hi!'])
      end

      it 'does not move down' do
        buffer.move_down
        expect_buffer_state_to_be(3, 2, ['hello there', '', 'hi!'])
      end
    end

    it 'moves to the end of the line on up arrow if it would have been past end of line' do
      buffer.move_up
      expect_buffer_state_to_be(0, 1, ['hello there', '', 'hi!'])
    end
  end

  describe 'scroll behavior' do
    subject(:buffer) do
      allow(mock_opened_file).to receive(:contents).and_return(test_content)
      described_class.new(mock_opened_file, context:)
    end

    before do
      buffer.show_gutter = true
    end

    describe 'scroll_down' do
      specify { expect(described_class).to be_allow_from_keymap(:scroll_down) }

      it 'scrolls down' do
        buffer.scroll_down
        output = buffer.output(Size.new(80, 24))

        expect(output.to_a.first).to eq(['   2  ', :line_number])
      end

      it 'causes cursor to scroll out of view' do
        buffer.scroll_down
        expect(buffer.cursor_position).to be_nil
      end

      it 'does not scroll past final line' do
        10.times { buffer.scroll_down }
        output = buffer.output(Size.new(80, 24))

        expect(output.to_a.first).to eq(['   4  ', :line_number])
      end
    end

    describe 'scroll_up' do
      before do
        10.times { buffer.scroll_down }
      end

      specify { expect(described_class).to be_allow_from_keymap(:scroll_up) }

      it 'scrolls up' do
        buffer.scroll_up
        output = buffer.output(Size.new(80, 24))

        expect(output.to_a.first).to eq(['   3  ', :line_number])
      end

      it 'does not scroll past first line' do
        10.times { buffer.scroll_up }
        output = buffer.output(Size.new(80, 24))

        expect(output.to_a.first).to eq(['   1  ', :line_number])
      end
    end

    describe 'cursor moves back into view' do
      before do
        10.times { buffer.scroll_down }
      end

      example 'when moving left' do
        buffer.move_left
        expect(buffer.cursor_position).not_to be_nil
      end

      example 'when moving right' do
        buffer.move_right
        expect(buffer.cursor_position).not_to be_nil
      end

      example 'when moving up' do
        buffer.move_up
        expect(buffer.cursor_position).not_to be_nil
      end

      example 'when moving down' do
        buffer.move_down
        expect(buffer.cursor_position).not_to be_nil
      end

      example 'when inserting' do
        buffer.insert('a')
        expect(buffer.cursor_position).not_to be_nil
      end

      example 'when backspacing' do
        buffer.delete_left
        expect(buffer.cursor_position).not_to be_nil
      end

      example 'when deleting' do
        buffer.delete_right
        expect(buffer.cursor_position).not_to be_nil
      end
    end
  end

  describe 'output' do
    subject(:buffer) do
      allow(mock_opened_file).to receive(:contents).and_return(test_content)
      described_class.new(mock_opened_file, context:)
    end

    before do
      buffer.show_gutter = true
    end

    example do
      output = buffer.output(Size.new(80, 24))

      expect(output.to_a).to eq(
        [
          ['   1  ', :line_number], ['The quick brown fox jumps over the lazy dog.', :text], ["\n", nil],
          ['   2  ', :line_number], ['...', :text], ["\n", nil],
          ['   3  ', :line_number], ["\n", nil],
          ['   4  ', :line_number], ['And everyone loved it!', :text], ["\n", nil]
        ] +
        [["\n", nil]] * 20
        # [tag("                                                                    row 1 col 1 \n", :status_line)]
      )
      expect(output.cursor).to eq(Position.new(6, 0))
    end

    example do
      output = buffer.output(Size.new(21, 24))

      expect(output.to_a).to eq(
        [
          ['   1  ', :line_number], ['The quick brow', :text], ['>', :wrap_symbol], ["\n", nil],
          ['      ', :line_number], ['n fox jumps ov', :text], ['>', :wrap_symbol], ["\n", nil],
          ['      ', :line_number], ['er the lazy do', :text], ['>', :wrap_symbol], ["\n", nil],
          ['      ', :line_number], ['g.', :text], ["\n", nil],
          ['   2  ', :line_number], ['...', :text], ["\n", nil],
          ['   3  ', :line_number], ["\n", nil],
          ['   4  ', :line_number], ['And everyone l', :text], ['>', :wrap_symbol], ["\n", nil],
          ['      ', :line_number], ['oved it!', :text], ["\n", nil]
        ] +
        [["\n", nil]] * 16
        # [tag("        row 1 col 1 \n", :status_line)]
      )
      expect(output.cursor).to eq(Position.new(6, 0))
    end
  end
end
