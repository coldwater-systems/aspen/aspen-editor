# Coordinates used in aspen-Editor

## Insertion Point Coordinates

The position of the insertion point within Aspen::Editor::Buffer is in logical
coordinates `[col, row]` as far as the buffer content is concerned. This
position is not affected by display attributes such as line wrapping or
scrolling. It defines the insertion point with the following subtleties:

- Within Aspen::Editor::Buffer `lines[row][col]` always defines the insertion
  point for anything that makes modification to the buffer, but `col` is
  calculated from `@col_virtual` inside BufferSelection.
- `@col_virtual` is something the user does not see, and only exists to allow
  the cursor to pass through shorter or blank lines:

  ![](passthru-short-line.gif)

- `@col_virtual` usually equals `col`, but the virtual column will be past the
  end of the line to remember how far to move the cursor rightward once the user
  moves to a longer line. Moving left or right, whether by moving the cursor, or
  by editing, will make `@col_virtual` within bounds again.

## Model and scrolled Coordinates

The model coordinate system is the first presentation-oriented system of a
component. The origin of model coordinates is at the top-left of the component's
content before scroll position is taken into account.

The origin of the scrolled coordinate system is the top-left of the component's
*visible* content. The coordinates are calculated by subtracting the scroll
position from model coordinates. Negative coordinates imply that the content has
been scrolled out of view in the left and/or up direction, and coordinates
larger than the component width/height have been scrolled out of view in the
right and/or down direction.

![](model-vs-scrolled.svg)

A component can have horizontal and vertical scrolling, which means it will have
both a `scroll_x` and `scroll_y` position. In other cases, there will be
vertical scrolling only, meaning the model width equals the component width, and
there's only a `scroll_y`. In components where scrolling is not allowed, then
the model size and component size are the same and the two origins are
the same.

## Screen Coordinates

This coordinate system is what the user sees.

Components are drawn onto the screen via `Aspen::TerminalOutput`. TerminalOutput
gives each component the width/height it can draw to, then the component
responds with the text that should go in the draw rectangle. In order to draw
the component to the screen, TerminalOutput moves the draw rectangle into
position. Component position and size are defined in screen coordinates.

Negative coordinates or coordinates beyond the console's width/height are
illegal. Overflow handling should be performed before sending text to
TerminalOutput.

## xterm Coordinates

This coordinate system should be considered an implementation detail of
`Aspen::Terminal`, but it is included here for completeness.

`Aspen::Terminal` is an abstraction on the kernel's tty that the editor is
attached to. The terminal emulator on the other end of the tty reports and
interprets coordinates in Y-X order and are 1-based (left margin and top margin
are 1, not 0), in accordance with the xterm protocol. `Aspen::Terminal`
abstracts this away so that all other parts of the editor speak in X-Y–ordered,
0-based coordinates.
