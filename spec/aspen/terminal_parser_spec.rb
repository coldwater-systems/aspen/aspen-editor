# frozen_string_literal: true

require 'aspen/terminal_parser'
require 'yaml'

RSpec.describe Aspen::TerminalParser do
  subject(:parser) { described_class.new }

  def expect_result(input, *expected)
    parser << input
    results = expected.map do
      parser.next_input
    end
    expect(parser).to be_eos
    expect(results).to eq(expected)
  end

  def expect_text(input, str) = expect_result(input, str)
  def expect_key(input, *mods, key_name) = expect_result(input, Aspen::Key.new(mods, key_name))

  describe 'handling of normal text input (in UTF-8)' do
    (' '..'~').each do |c|
      example { expect_text(c, c) }
    end
    example { expect_text('é', 'é') }
    example { expect_text('ü', 'ü') }
    example { expect_text('…', '…') }
    example { expect_text('😀', '😀') }
  end

  xdescribe 'Paste bracketing' do
    it 'handles bracketing over a single read' do
      expect_result("\e[200~Some text\e[201~", Aspen::Editor::InputMethod::PastedText.new('Some text'))
    end

    it 'handles bracketing over multiple partial reads' do
      scanner = StringScanner.new("\e[200~To be continued...\n".dup)
      expect(described_class.parse_rawkeys(scanner).to_a).to be_empty
      expect(scanner.string).to eq("\e[200~To be continued...\n")

      scanner << "and now the thrilling conclusion\e[201~"
      expect(described_class.parse_rawkeys(scanner).to_a).to eq([
        Aspen::Editor::InputMethod::PastedText.new("To be continued...\nand now the thrilling conclusion"),
      ])
      expect(scanner).to be_eos
    end
  end

  describe 'handling of conventional C-? sequences' do
    example { expect_key("\x00", :ctrl, :space) } # aka ^@
    example { expect_key("\x01", :ctrl, 'A') }
    example { expect_key("\x02", :ctrl, 'B') }
    example { expect_key("\x03", :ctrl, 'C') }
    example { expect_key("\x04", :ctrl, 'D') }
    example { expect_key("\x05", :ctrl, 'E') }
    example { expect_key("\x06", :ctrl, 'F') }
    example { expect_key("\x07", :ctrl, 'G') }
    example { expect_key("\x08", :ctrl, 'H') }
    example { expect_key("\t", :tab) }         # aka ^I
    example { expect_key("\x0A", :ctrl, 'J') }
    example { expect_key("\x0B", :ctrl, 'K') }
    example { expect_key("\x0C", :ctrl, 'L') }
    example { expect_key("\r", :enter) }       # aka ^M
    example { expect_key("\x0E", :ctrl, 'N') }
    example { expect_key("\x0F", :ctrl, 'O') }
    example { expect_key("\x10", :ctrl, 'P') }
    example { expect_key("\x11", :ctrl, 'Q') }
    example { expect_key("\x12", :ctrl, 'R') }
    example { expect_key("\x13", :ctrl, 'S') }
    example { expect_key("\x14", :ctrl, 'T') }
    example { expect_key("\x15", :ctrl, 'U') }
    example { expect_key("\x16", :ctrl, 'V') }
    example { expect_key("\x17", :ctrl, 'W') }
    example { expect_key("\x18", :ctrl, 'X') }
    example { expect_key("\x19", :ctrl, 'Y') }
    example { expect_key("\x1A", :ctrl, 'Z') }
    example { expect_key("\e", :esc) }         # aka ^[
    example { expect_key("\x1C", :ctrl, '\\') }
    example { expect_key("\x1D", :ctrl, ']') }
    example { expect_key("\x1E", :ctrl, :shift, '6') }
    example { expect_key("\x1F", :ctrl, :shift, '-') }
    example { expect_key("\x7F", :backspace) } # aka ^?
  end

  describe 'handling of conventional M-? sequences' do
    ('a'..'z').each do |c|
      example { expect_key("\e#{c}", :alt, c.upcase) }
    end
    '[]\;\',./`'.each_char do |c|
      example { expect_key("\e#{c}", :alt, c) }
    end
    ('A'..'Z').each do |c|
      example { expect_key("\e#{c}", :alt, :shift, c) }
    end
    example { expect_key("\e!", :alt, :shift, '1') }
    example { expect_key("\e@", :alt, :shift, '2') }
    example { expect_key("\e#", :alt, :shift, '3') }
    example { expect_key("\e$", :alt, :shift, '4') }
    example { expect_key("\e%", :alt, :shift, '5') }
    example { expect_key("\e^", :alt, :shift, '6') }
    example { expect_key("\e&", :alt, :shift, '7') }
    example { expect_key("\e*", :alt, :shift, '8') }
    example { expect_key("\e(", :alt, :shift, '9') }
    example { expect_key("\e)", :alt, :shift, '0') }
    example { expect_key("\e_", :alt, :shift, '-') }
    example { expect_key("\e+", :alt, :shift, '=') }
    example { expect_key("\e{", :alt, :shift, '[') }
    example { expect_key("\e}", :alt, :shift, ']') }
    example { expect_key("\e|", :alt, :shift, '\\') }
    example { expect_key("\e:", :alt, :shift, ';') }
    example { expect_key("\e\"", :alt, :shift, "'") }
    example { expect_key("\e<", :alt, :shift, ',') }
    example { expect_key("\e>", :alt, :shift, '.') }
    example { expect_key("\e?", :alt, :shift, '/') }
    example { expect_key("\e~", :alt, :shift, '`') }
  end

  describe 'handling of conventional M-C-? sequences' do
    example { expect_key("\e\x01", :ctrl, :alt, 'A') }
    example { expect_key("\e\x02", :ctrl, :alt, 'B') }
    example { expect_key("\e\x03", :ctrl, :alt, 'C') }
    example { expect_key("\e\x04", :ctrl, :alt, 'D') }
    example { expect_key("\e\x05", :ctrl, :alt, 'E') }
    example { expect_key("\e\x06", :ctrl, :alt, 'F') }
    example { expect_key("\e\x07", :ctrl, :alt, 'G') }
    example { expect_key("\e\x08", :ctrl, :alt, 'H') }
    example { expect_key("\e\t", :alt, :tab) }         # aka M-C-I
    example { expect_key("\e\x0A", :ctrl, :alt, 'J') }
    example { expect_key("\e\x0B", :ctrl, :alt, 'K') }
    example { expect_key("\e\x0C", :ctrl, :alt, 'L') }
    example { expect_key("\e\r", :alt, :enter) }       # aka M-C-M
    example { expect_key("\e\x0E", :ctrl, :alt, 'N') }
    example { expect_key("\e\x0F", :ctrl, :alt, 'O') }
    example { expect_key("\e\x10", :ctrl, :alt, 'P') }
    example { expect_key("\e\x11", :ctrl, :alt, 'Q') }
    example { expect_key("\e\x12", :ctrl, :alt, 'R') }
    example { expect_key("\e\x13", :ctrl, :alt, 'S') }
    example { expect_key("\e\x14", :ctrl, :alt, 'T') }
    example { expect_key("\e\x15", :ctrl, :alt, 'U') }
    example { expect_key("\e\x16", :ctrl, :alt, 'V') }
    example { expect_key("\e\x17", :ctrl, :alt, 'W') }
    example { expect_key("\e\x18", :ctrl, :alt, 'X') }
    example { expect_key("\e\x19", :ctrl, :alt, 'Y') }
    example { expect_key("\e\x1A", :ctrl, :alt, 'Z') }
    example { expect_key("\e\e", :alt, :esc) }         # aka M-C-[
    example { expect_key("\e\x1C", :ctrl, :alt, '\\') }
    example { expect_key("\e\x1D", :ctrl, :alt, ']') }
    example { expect_key("\e\x1E", :ctrl, :alt, :shift, '6') }
    example { expect_key("\e\x1F", :ctrl, :alt, :shift, '-') }
    example { expect_key("\e\x7F", :alt, :backspace) } # aka M-C-?
  end

  {
    'iTerm (CSI u)'        => '../terminals/iterm-csiu.yml',
    'iTerm'                => '../terminals/iterm.yml',
    "Apple's Terminal.app" => '../terminals/apple-terminal.yml',
  }.each do |terminal_name, terminal_data_filename|
    sequences = YAML.safe_load_file(File.expand_path(terminal_data_filename, __dir__))
    sequences.each do |key_name, raw_input|
      it "is able to interpret terminal sequences of #{terminal_name} for #{key_name}" do
        parser << raw_input
        result = parser.next_input

        expect(parser).to be_eos
        expect(result).to be_a(Aspen::Key)
        expect(result).to eq(Aspen::Key.parse(key_name))

        # test Key#to_s while we're here to make sure #parse and #to_s are symmetric
        expect(result.to_s).to eq(key_name)
      end
    end
  end

  # TODO: run exe/terminal_keys on this terminal
  # describe 'Terminal behavior of Linux local terminal' do
  #   xexample { expect_key("\e[[A", :f1) }
  #   xexample { expect_key("\e[[B", :f2) }
  #   xexample { expect_key("\e[[C", :f3) }
  #   xexample { expect_key("\e[[D", :f4) }
  #   xexample { expect_key("\e[[E", :f5) }
  #   example { expect_key("\e[17~", :f6) }
  #   example { expect_key("\e[18~", :f7) }
  #   example { expect_key("\e[19~", :f8) }
  #   example { expect_key("\e[20~", :f9) }
  #   example { expect_key("\e[21~", :f10) }
  #   example { expect_key("\e[23~", :f11) }
  #   example { expect_key("\e[24~", :f12) }
  #   example { expect_key("\e[A", :up) }
  #   example { expect_key("\e[B", :down) }
  #   example { expect_key("\e[D", :left) }
  #   example { expect_key("\e[C", :right) }
  #   example { expect_key("\e[3~", :delete) }
  #   example { expect_key("\e[2~", :insert) }
  #   example { expect_key("\e[1~", :home) }
  #   example { expect_key("\e[4~", :end) }
  #   example { expect_key("\e[5~", :page_up) }
  #   example { expect_key("\e[6~", :page_down) }
  #   xexample { expect_key("\e[25~", :shift, :f1) }
  #   xexample { expect_key("\e[26~", :shift, :f2) }
  #   xexample { expect_key("\e[28~", :shift, :f3) }
  #   xexample { expect_key("\e[29~", :shift, :f4) }
  #   xexample { expect_key("\e[31~", :shift, :f5) }
  #   xexample { expect_key("\e[32~", :shift, :f6) }
  #   xexample { expect_key("\e[33~", :shift, :f7) }
  #   xexample { expect_key("\e[34~", :shift, :f8) }
  # end
end
