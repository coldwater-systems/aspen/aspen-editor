# frozen_string_literal: true

require_relative 'lib/aspen/editor/pkginfo'

Gem::Specification.new do |spec|
  spec.name        = 'aspen-editor'
  spec.version     = Aspen::Editor::VERSION
  spec.summary     = 'A CLI editor part of the Aspen suite of core tools'
  spec.description = 'A text editor that aims to bring the muscle memory of GUI text editing ' \
                     '(the familiar ctrl-C/X/V, click and drag, etc.) into the terminal.'
  spec.author      = 'Alex Gittemeier'
  spec.email       = 'me@a.lexg.dev'
  spec.license     = 'GPL-3.0-only'

  spec.metadata['homepage_uri']      = 'https://gitlab.com/coldwater-systems/aspen/aspen-editor'
  spec.metadata['source_code_uri']   = 'https://gitlab.com/coldwater-systems/aspen/aspen-editor/-/tree/stable'
  spec.metadata['documentation_uri'] = 'https://www.rubydoc.info/gems/aspen-editor/'
  spec.metadata['bug_tracker_uri']   = 'https://gitlab.com/coldwater-systems/aspen/aspen-editor/-/issues'
  spec.metadata['changelog_uri']     = 'https://gitlab.com/coldwater-systems/aspen/aspen-editor/-/commits/stable'
  spec.metadata['rubygems_mfa_required'] = 'true'
  spec.homepage = spec.metadata['homepage_uri']

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:bin|test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir = 'exe'
  spec.executables = spec.files.grep(%r{\Aexe/}) {|f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.required_ruby_version = '>= 3.2.0'

  spec.add_dependency 'colorize', '~> 1.1'
  spec.add_dependency 'pastel', '~> 0.8'
end
